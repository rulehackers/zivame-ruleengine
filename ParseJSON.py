import json

'''
[
    { "id":"2","category_id":["200","201"], "price":"2000" }
    ,{ "id":"2","category_id":["200","201"], "price":"2000" }
]
'''
         
class JSONParser(object):
    def __init__(self):
        self.json_dump = ""
        self.cart_total = 0
    
    def parseData(self, filename="data_input.json"):
        total = 0 
        json_file = open(filename,"r")
        json_dump = json.load(json_file)
        print "First element",json_dump[0]
        print "JSON Size", len(json_dump)
        for x in range(0,len(json_dump)):
            #print json_dump[x]['product']['id']
            #print json_dump[x]['product']['category_id']
            total = total + float(json_dump[x]['product']['price'])
        print "------"*5
        self.json_dump = json_dump
        self.cart_total = total
        print self.cart_total
        return json_dump
        
if __name__=="__main__":
    print "JSON Parse"
    print 
    print ParseJSON().parseData("/Users/pradeepbishnoi/Projects/BitzerMobile/Zivame/rulehackers/zivame-ruleengine/data_input.json")
    #.parseData()
