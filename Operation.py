class Operation():
    '''To perform binary operation over input data'''

    __greaterThan = '>'
    __greaterThanEqualTo = '>='
    __lessThan = '<'
    __lessThanEqualTo = '<='
    __notEqualTo = '!='
    __equalTo = '='
    __or = 'or'
    __and = 'and'
    __in = 'in'
    __notin = 'notin'

    def greaterThan(self, entity, value):
        if entity > value:
            return True
        else:
            return False
    
    def lessThan(self, entity, value):
        print "TYPES:",type(entity),type(value)
        if entity < value:
            return True
        else:
            return False
    
    def equalTo(self, entity, value):
        if entity == value:
            return True
        else:
            return False
        
    def notEqualTo(self, entity, value):
        if entity != value:
            return True
        else:
            return False

    def greaterThanEqualTo(self, entity, value):
        return self.greaterThan(entity,value) or self.equalTo(entity,value)

    def lessThanEqualTo(self, entity, value):
        return self.lessThan(entity,value) or self.equalTo(entity,value)

    def oring(self, left, right):
        return left or right

    def anding(self, left, right):
        return left and right

    def existsIn(self, value, listOfItems):
        if type(value) is list:
            return set(value).issubset(set(listOfItems))
        else:
            return value in listOfItems

    def doesNotExistIn(self,value,listOfItems):
        return not(self.existsIn(value,listOfItems))

    def operate(self,entity,operator,value):
        ''' operate using the member functions over entity and value which are in custome formats ''' 
        
        # handling different entity formats
        if type(entity) is list:
            entity = [ int(i) for i in entity]
        else:
            print "entity: ",entity
            entity = float(entity)

        # handling different value formats 
        if type(value) is bool:
            pass
        elif value[0] == '(':
            valueStringList = value.replace('(','').replace(')','').split(',')
            valueList = [ int(i) for i in valueStringList]
        else:
            value = float(value)

        # perform operation based on the operator we have
        if operator == self.__equalTo:
            return self.equalTo(entity,value)
        elif operator == self.__notEqualTo:
            return self.notEqualTo(entity,value)
        elif operator == self.__greaterThan:
            return self.greaterThan(entity,value)
        elif operator == self.__greaterThanEqualTo:
            return self.greaterThanEqualTo(entity,value)
        elif operator == self.__lessThan:
            return self.lessThan(entity,value)
        elif operator == self.__lessThanEqualTo:
            return self.lessThanEqualTo(entity,value)
        elif operator == self.__or:
            return self.oring(entity,value)
        elif operator == self.__and:
            return self.anding(entity,value)
        elif operator == self.__in:
            return self.existsIn(entity,valueList)
        elif operator == self.__notin:
            return self.doesNotExistIn(entity,valueList)

if __name__ == "__main__":
    ''' test code '''
    operate = Operation()

    price = 1000
    print operate.lessThan(price,2000)
    print operate.greaterThan(price,200)
    print operate.greaterThanEqualTo(price,100)
    print operate.equalTo(price,200)
    
    print operate.lessThanEqualTo(price,100)
    print operate.notEqualTo(price,100)
    print operate.notEqualTo(price,40)
    print operate.oring(True,False)
    print operate.anding(True,False)
    print operate.existsIn(1,[1,2,3])