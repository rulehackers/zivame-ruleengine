# README #

ZIVHACK 2014 - RuleEngine in Python by Abhishek & Pradeep [RuleHackers]

A Rule Engine from scratch with a User Interface for entering the rules :)

### What is this repository for? ###

* Quick summary
To get started with Building the Rule Engine from Scratch. Our rule engine evalutes the rule using INFIX expression evalution. 

### How to try that? ###
* Access Heroku instance at [ Rule-Engine ](https://rule-engine.herokuapp.com)

### How does it look ###
![Screen after applying the JSON data](https://bytebucket.org/rulehackers/zivame-ruleengine/raw/b81f2ab9010259208099a01809ee62c87446bcd4/screenshots/1.png)

![Applying the rule data IF & THEN condition](https://bytebucket.org/rulehackers/zivame-ruleengine/raw/b81f2ab9010259208099a01809ee62c87446bcd4/screenshots/2.png)

![Final evaluted Rule output](https://bytebucket.org/rulehackers/zivame-ruleengine/raw/b81f2ab9010259208099a01809ee62c87446bcd4/screenshots/3.png)

### How do I get set up? ###

* Summary of set up
Setup python & flask (module).

* Configuration
No additional configuration required.

* Dependencies
Require Python 2.7, Flask (python module)

* Deployment instructions
Run the "python webserver.py" and it will start the Web server on default port "5000".
Sample data is available in files (data_input.json & rule_data.txt)

### Who do I talk to? ###

* Abhishek Nair (abhishek.alchemist@gmail.com)
* Pradeep Bishnoi (pradeepbishnoi@gmail.com)
